import com.beust.klaxon.Json
import com.beust.klaxon.JsonArray
import java.io.File
import com.beust.klaxon.Klaxon
import java.net.URL

//King entity.
// Json(name = "") means that we use custom property name for value for key in ""
data class King (@Json(name = "nm") val name: String,
                 @Json(name = "cty") val city: String,
                 @Json(name = "hse") val house: String,
                 @Json(name = "yrs") val years: String)

fun main(args: Array<String>) {
    val kingsJSON = URL("http://mysafeinfo.com/api/data?list=englishmonarchs&format=json").readText()
    val result = Klaxon().parseArray<King>(kingsJSON)// Transformating text from line 21 to the List of OBJECTS of class King
    val athelstanKing = result!!.filter {
        it.name.contains("athelstan", ignoreCase = true)
    }
    println(athelstanKing)
}
